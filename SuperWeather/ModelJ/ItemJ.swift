//
//  Item.swift
//  SuperWeather
//
//  Created by Jossué Dután on 17/7/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import Foundation
import RealmSwift

class ItemJ: Object {
    @objc dynamic var id:String?
    @objc dynamic var cityName:String?
    @objc dynamic var itemDescription:String?
    @objc dynamic var done = false
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
