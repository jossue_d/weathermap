//
//  JossueWeather.swift
//  SuperWeather
//
//  Created by Jossué Dután on 30/5/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import Foundation

/* En json suponer:
 "main_weather":"rain"
 let mainWeather:String
 enum CodingKeys: String, CodingKey{
    case weather
    case mainWeather = "main_weather"
 }
 */

struct JossueWeatherInfo: Decodable {
    let weather: [JossueWeather]
    let name: String
    
    
    
}
struct JossueWeather: Decodable {
    let id:Int
    let description:String
}
